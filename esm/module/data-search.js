/**
 * Searches for data based on the given event.
 *
 * @param {Event} event - The event object triggered by the search.
 * @return {boolean} Returns true if the search is successful; otherwise, false.
 */
const dataSearch = (event) => {
  const form = event.target

  if (!(form instanceof HTMLFormElement)) {
    return false
  }

  const input = form.querySelector('[name=search]')

  if (!input?.value) {
    return false
  }

  const searchEngine = form.getAttribute('data-search') || null

  switch (searchEngine) {
    case 'google':
      return googleSearch(form, input)
    default:
      return true
  }
}

/**
 * Updates the form action and method to perform a Google search.
 *
 * @param {HTMLFormElement} form - The form element to update.
 * @param {HTMLInputElement} input - The input element containing the search query.
 * @return {boolean} Returns true after updating the form.
 */
const googleSearch = (form, input) => {
  form.setAttribute('action', 'https://www.google.com/search')
  form.setAttribute('method', 'get')

  const searchQueryName = 'q'
  const inputValue = input.value
  const newSearchQueryValue = `${inputValue} inurl:${location.hostname}`

  input.setAttribute('name', searchQueryName)
  input.value = newSearchQueryValue

  resetInput(input, inputValue)

  return true
}

/**
 * Resets the input element value and sets the 'name' attribute after a delay of 300 milliseconds.
 *
 * @param {HTMLElement} inputElement - The input element to reset the value of.
 * @param {string} inputValue - The value to set the input element to.
 */
const resetInput = (inputElement, inputValue) => {
  setTimeout(() => {
    inputElement.value = inputValue
    inputElement.setAttribute('name', 'search')
  }, 300)
}

export default dataSearch
