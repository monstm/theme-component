/**
 * Toggles the 'toggled' class on elements based on the value of the 'data-toggle' attribute of the event target.
 *
 * @param {Event} event - The event object that triggered the function.
 * @return {undefined} This function does not return a value.
 */
const dataToggle = (event) => {
  const target = event.target.closest('[data-toggle]')
  if (!target) return

  const selectors = target.getAttribute('data-toggle')
  const elements = document.querySelectorAll(selectors)
  elements.forEach((element) => {
    element.classList.toggle('toggled')
  })
}

export default dataToggle
