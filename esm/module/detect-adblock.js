/**
 * Detects if an adblock is enabled by adding a script element to the head element and checking if a specific key-value pair is set.
 *
 * @param {HTMLHeadElement} headElement - The head element to append the script element to.
 * @param {HTMLBodyElement} bodyElement - The body element to modify if the adblock is detected.
 * @return {void} This function does not return a value.
 */
const detectAdblock = (headElement, bodyElement) => {
  const generateRandomString = () => Math.random().toString(36).substring(2)

  const key = '_' + generateRandomString()
  const value = generateRandomString()

  const scriptElement = document.createElement('script')
  scriptElement.type = 'text/javascript'
  scriptElement.src = 'https://cdn.jsdelivr.net/npm/theme-component@1/js/adlib.js'
  scriptElement.dataset.key = key
  scriptElement.dataset.value = value

  const checkAdblock = () => {
    if (window[key] !== value) {
      if (headElement instanceof HTMLHeadElement) {
        headElement.innerHTML = ''
      }

      if (bodyElement instanceof HTMLBodyElement) {
        bodyElement.innerHTML = '<h1>Adblock Detected</h1><p>Please disable your adblock</p>'
      }
    }
  }

  scriptElement.addEventListener('load', checkAdblock)
  scriptElement.addEventListener('error', checkAdblock)

  headElement.appendChild(scriptElement)
}

export default detectAdblock
