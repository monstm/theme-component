import dataToggle from './module/data-toggle.js'
import dataSearch from './module/data-search.js'
import detectAdblock from './module/detect-adblock.js'

const head = document.querySelector('head')
const body = document.querySelector('body')

window.addEventListener('load', () => {
  detectAdblock(head, body)
})

body.addEventListener('click', (event) => {
  dataToggle(event)
})

body.addEventListener('submit', (event) => {
  dataSearch(event)
})
