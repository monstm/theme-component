# Theme Component

## Pages

* [Data Search](https://monstm.gitlab.io/theme-component/data-search.html)
* [Data Toggle](https://monstm.gitlab.io/theme-component/data-toggle.html)
* [Detect Adblock](https://monstm.gitlab.io/theme-component/detect-adblock.html)

## Markup Validation

* [Data Search](https://validator.w3.org/nu/?doc=https%3A%2F%2Fmonstm.gitlab.io%2Ftheme-component%2Fdata-search.html)
* [Data Toggle](https://validator.w3.org/nu/?doc=https%3A%2F%2Fmonstm.gitlab.io%2Ftheme-component%2Fdata-toggle.html)
* [Detect Adblock](https://validator.w3.org/nu/?doc=https%3A%2F%2Fmonstm.gitlab.io%2Ftheme-component%2Fdetect-adblock.html)
